#!/usr/bin/env python3

# -----------
# Imports
# -----------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_print
# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ------
    # read
    # ------

    def test_read(self):
        s = "A Madrid Hold"
        i = diplomacy_read(s)
        self.assertEqual(i, ["A", "Madrid", "Hold", None])

    def test_read_1(self):
        s = "A London Move Paris"
        i = diplomacy_read(s)
        self.assertEqual(i, ["A", "London", "Move", "Paris"])

    def test_read_2(self):  # this is supposed to fail
        s = "A London Move"
        foo = False
        try:
            i = diplomacy_read(s)
        except ValueError:
            foo = True
        self.assertEqual(foo, True)

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Bardelona Move Madrid\n C Austin Move Madrid\n D Ted Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Ted\n")

    def test_solve_4(self):
        r = StringIO(
            "A Madrid Hold\nB Stockholm Move Madrid\nC Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC Paris\n")

    def test_solve_5(self):
        r = StringIO(
            "A Madrid Hold\nB Stockholm Move Madrid\nC Paris Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC Paris\n")

    def test_solve_6(self):
        r = StringIO(
            "A Jeff Move Madrid\nB Stockholm Move Madrid\nC Paris Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC Paris\n")

    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Madrid Hold")
        w = StringIO()
        foo = False
        try:
            diplomacy_solve(r, w)
        except:
            foo = True
            print(w.getvalue())
        self.assertEqual(foo, True)

    def test_solve_8(self):
        r = StringIO("A Madrid Hold\nA Paris Hold")
        w = StringIO()
        foo = False
        try:
            diplomacy_solve(r, w)
        except:
            foo = True
            print(w.getvalue())
        self.assertEqual(foo, True)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        input = {"A": "Madrid", "B": "Austin", "C": "Town"}
        diplomacy_print(w, input)
        self.assertEqual(w.getvalue(), "A Madrid\nB Austin\nC Town\n")

    def test_print_1(self):
        w = StringIO()
        input = {"A": "Horse", "B": "Jorge", "C": "City"}
        diplomacy_print(w, input)
        self.assertEqual(w.getvalue(), "A Horse\nB Jorge\nC City\n")

    def test_print_2(self):
        w = StringIO()
        input = {"Jerald": "Lemon", "Jimmy": "Jorge", "Calvin": "City"}
        diplomacy_print(w, input)
        self.assertEqual(
            w.getvalue(), "Jerald Lemon\nJimmy Jorge\nCalvin City\n")
# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()
