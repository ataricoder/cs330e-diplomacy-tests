#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, Army

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold"
        i = diplomacy_read(s)
        j = {"A": Army("A", "Madrid", "Hold", None, 0)}
        self.assertEqual(i, j)

    def test_read_1(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B"
        i = diplomacy_read(s)
        j = {"A": Army("A", "Madrid", "Hold", None, 0), "B": Army("B", "Barcelona", "Move", "Madrid", 0),
             "C": Army("C", "London", "Support", "B", 0)}
        self.assertEqual(i, j)

    def test_read_2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid"
        i = diplomacy_read(s)
        j = {"A": Army("A", "Madrid", "Hold", None, 0), "B": Army("B", "Barcelona", "Move", "Madrid", 0)}
        self.assertEqual(i, j)

    def test_read_3(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London"
        i = diplomacy_read(s)
        j = {"A": Army("A", "Madrid", "Hold", None, 0), "B": Army("B", "Barcelona", "Move", "Madrid", 0), "C": Army("C", "London", "Support", "B", 0), "D": Army("D", "Austin", "Move", "London", 0)}
        self.assertEqual(i, j)

    # ----
    # eval
    # ----

    def test_eval_1(self): #one army holding
        s = {"A": Army("A", "Madrid", "Hold")}
        v = diplomacy_eval(s)
        self.assertEqual(v, "A Madrid\n")

    def test_eval_2(self): #two armies fight, the one with support wins
        s = {"A": Army("A", "Madrid", "Hold"), "D": Army("D", "Barcelona", "Move", "Madrid"),
             "H": Army("H", "London", "Support", "D")}
        v = diplomacy_eval(s)
        self.assertEqual(v, "A [dead]\nD Madrid\nH London\n")

    def test_eval_3(self): #four armies fighting over one location, no support, all die
        s = {"A": Army("A", "Madrid", "Hold"), "B": Army("B", "Barcelona", "Move", "Madrid"), "U": Army("U", "Chicago", "Move", "Madrid"), "P": Army("P", "Plano", "Move", "Madrid")}
        v = diplomacy_eval(s)
        self.assertEqual(v, "A [dead]\nB [dead]\nP [dead]\nU [dead]\n")

    def test_eval_4(self): #2 armies fight, one has support but supporter is attacked
        s = {"A": Army("A", "Madrid", "Hold"), "B": Army("B", "Barcelona", "Move", "Madrid"), "C": Army("C", "London", "Support", "B"), "D": Army("D", "Austin", "Move", "London")} 
        v = diplomacy_eval(s)
        self.assertEqual(v, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_eval_5(self): #2 armies swap places
        s = {"Z": Army("Z", "Madrid", "Move", "Frisco"), "V": Army("V", "Frisco", "Move", "Madrid")} 
        v = diplomacy_eval(s)
        self.assertEqual(v, "V Madrid\nZ Frisco\n")
    
    def test_eval_6(self): #2 armies fight, one with support wins, the fourth moves
        s = {"Q": Army("Q", "Beijing", "Support", "R"), "R": Army("R", "Rochester", "Hold"), "S": Army("S", "Detroit", "Move", "Seattle"), "T": Army("T", "Seattle", "Move", "Rochester")} 
        v = diplomacy_eval(s)
        self.assertEqual(v, "Q Beijing\nR Rochester\nS Seattle\nT [dead]\n")

    def test_eval_7(self): #2 armies fight, the other moves
        s = {"A": Army("A", "Madrid", "Hold"), "B": Army("B", "Barcelona", "Move", "Madrid"),
             "C": Army("C", "London", "Move", "Barcelona")}
        v = diplomacy_eval(s)
        self.assertEqual(v, "A [dead]\nB [dead]\nC Barcelona\n")

    def test_eval_8(self): #one army holds, the other supports
        s = {"A": Army("A", "Rome", "Hold"), "B": Army("B", "Barcelona", "Support", "A")}
        v = diplomacy_eval(s)
        self.assertEqual(v, "A Rome\nB Barcelona\n")

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        sol = "A Madrid\n"
        diplomacy_print(w, sol)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_1(self):
        w = StringIO()
        sol = "A [dead]\nB Madrid\nC London Support B\n"
        diplomacy_print(w, sol)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London Support B\n")

    def test_print_2(self):
        w = StringIO()
        sol = "A [dead]\nB [dead]\n"
        diplomacy_print(w, sol)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print_3(self):
        w = StringIO()
        sol = "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        diplomacy_print(w, sol)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
